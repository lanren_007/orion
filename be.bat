@ECHO OFF
@chcp 65001>nul

if "%1"=="dev" goto dev
if "%1"=="d" goto dev

if "%1"=="open" goto open
if "%1"=="o" goto open

goto help

:dev
  gin -port 8081 -appPort 8082 ^
    -path .\backend\src\ -bin .\backend\gin-bin ^
    run .\backend\src\main.go
goto end

:open
  start C:\Users\东东\AppData\Local\Postman\Postman.exe
goto end

:help
  echo #  %0 version 2024.05.31
  echo #    后端快捷脚本
  echo #
  echo #  command:
  echo #    %0 help
  echo #
  echo #  options:
  echo #    d dev // 后端以开发模式启动
  echo #    o open // open Postman
goto end

:end