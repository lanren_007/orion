-- 数据库初始化

CREATE DATABASE IF NOT EXISTS orion
	CHARACTER SET = 'utf8'
	COLLATE = 'utf8_general_ci'
;

USE orion;

CREATE TABLE IF NOT EXISTS users (
	id		INT NOT NULL AUTO_INCREMENT,
	username	VARCHAR(100) NOT NULL,
  email VARCHAR(100) NOT NULL,
	PRIMARY KEY (id)
);

-- 插入几条测试数据
INSERT INTO users (username, email) VALUES ('user1', 'user1@example.com');
INSERT INTO users (username, email) VALUES ('user2', 'user2@example.com');

CREATE TABLE IF NOT EXISTS passwords (
	id			INT NOT NULL AUTO_INCREMENT,
	password 	VARCHAR(32) NOT NULL,
	PRIMARY KEY (id)
);
