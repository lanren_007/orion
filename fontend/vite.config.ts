import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// ElementPlus 插件, 自动导入组件和图标
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    AutoImport({
      // 自动导入 vue 相关函数
      imports: ['vue'],
      resolvers: [
        ElementPlusResolver(),
        // 自动导入图标组件
        IconsResolver()
      ]
    }),
    Components({
      resolvers: [
        ElementPlusResolver(),
        // 自动注册图标组件
        IconsResolver({
          enabledCollections: ['ep']
        })
      ]
    }),
    Icons({
      autoInstall: true
    })
  ],
  server: {
    port: 8079,
    proxy: {
      '/user': 'http://localhost:8081/'
    },
  },
  publicDir: '../public/',
  build: {
    // 发行路径
    // outDir: "../ops/fontend",
    outDir: "dist",
  },
})
