import lf from "localforage";

lf.config({
  name: "orion",
});

export function init() {
  lf.getItem("scaffold-main")
    .then(value => {
      if (value === null) {
        lf.setItem("scaffold-main", {
          name: "Default Title",
          items: [{ title: "Default bookmark", url: "www.example.com" }],
        })
          .then(value => {
            console.log("数据库初始化成功", value);
          })
          .catch(err => {
            console.error("init localforage: 写入数据时出错", err);
          });
      } else {
        console.log("数据库已存在", value);
      }
    })
    .catch(err => {
      console.error("init localforage error!", err);
    });
}
