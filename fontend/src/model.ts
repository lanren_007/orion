export interface BookmarkModel {
  title: string;
  url: string;
}

export interface ScaffoldModel {
  title: string;
  content: Array<BookmarkModel>;
}
