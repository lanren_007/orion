@ECHO OFF
@chcp 65001>nul

if "%1"=="dev" goto dev
if "%1"=="d" goto dev

if "%1"=="build" goto build
if "%1"=="b" goto build

if "%1"=="preview" goto preview
if "%1"=="p" goto preview

goto help

:dev
  yarn --cwd ./fontend dev
goto end

:build
  yarn --cwd ./fontend build
goto end

:preview
  yarn --cwd ./fontend preview
goto end

:help
  echo #  %0 version 2024.06.05
  echo #    前端快捷脚本
  echo #
  echo #  command:
  echo #    %0 help
  echo #
  echo #  options:
  echo #    d dev // 前端以开发模式启动
  echo #    b build
  echo #    p preview
goto end

:end