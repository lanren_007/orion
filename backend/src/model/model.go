// package model
package model

type Users struct {
	Id       int    `gorm:"column:id;type:int(11);primary_key;AUTO_INCREMENT" json:"id"`
	Username string `gorm:"column:username;type:varchar(100);NOT NULL" json:"username"`
	Email    string `gorm:"column:email;type:varchar(100);NOT NULL" json:"email"`
}
