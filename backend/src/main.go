package main

import (
	// "fmt"

	// "github.com/gin-ginic/gin"
	"orion-backend/router"
)

func main() {
	r := router.Router()

	// r.Run(":8081") // 发布后使用的端口
	r.Run(":8082") // 开发时热更新工具 gin 占用了 8081， 所以用 8082
}
