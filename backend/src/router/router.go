package router

import (
	"log"
	"net/http"

	"github.com/gin-gonic/gin"

	"orion-backend/model"
)

func Router() *gin.Engine {
	r := gin.Default()

	db, err := model.ConnectToDatabase()
	if err != nil {
		log.Fatal(err)
	}

	user := r.Group("/user")
	{
		user.POST("", func(ctx *gin.Context) {
			ctx.JSON(http.StatusOK, "{}")
		})
		user.GET("", func(ctx *gin.Context) {
			var user model.Users
			db.First(&user)
			ctx.JSON(http.StatusOK, user)
		})
		user.PUT("", func(ctx *gin.Context) {
			ctx.JSON(http.StatusOK, "{}")
		})
		user.PATCH("", func(ctx *gin.Context) {
			ctx.JSON(http.StatusOK, "{}")
		})
		user.DELETE("", func(ctx *gin.Context) {
			ctx.JSON(http.StatusOK, "{}")
		})
		user.OPTIONS("", func(ctx *gin.Context) {
			ctx.JSON(http.StatusOK, "{}")
		})
	}

	return r
}
