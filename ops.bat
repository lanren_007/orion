@ECHO OFF
@chcp 65001>nul

if "%1"=="task" goto task
if "%1"=="t" goto task

if "%1"=="start" goto start
if "%1"=="s" goto start

if "%1"=="reload" goto reload
if "%1"=="r" goto reload

if "%1"=="quit" goto quit
if "%1"=="q" goto quit

if "%1"=="fix" goto fix
if "%1"=="f" goto fix

if "%1"=="open" goto open
if "%1"=="o" goto open

if "%1"=="ip" goto ip
if "%1"=="i" goto ip

if "%1"=="edit" goto edit
if "%1"=="e" goto edit

if "%1"=="config" goto config
if "%1"=="c" goto config

goto help

:task
  echo tasklist:
  tasklist | findstr nginx
goto end

:start
  if not exist .\ops\logs\nginx.pid (
    start /b .\ops\bin\nginx.exe -p .\ops
    start http://localhost:8080
  ) else (
    echo Nginx 正在运行或意外退出，运行 nginx task 查看进程
  )
goto end

:reload
  .\ops\bin\nginx.exe -p .\ops -s reload
goto end

:quit
  .\ops\bin\nginx.exe -p .\ops -s quit
goto end

:fix
  del .\ops\logs\nginx.pid
goto end

:open
  start http://localhost:8080
goto end

:ip
  start http://127.0.0.1:8080
goto end

:edit
  code .\nginx.bat
goto end

:config
  code .\ops\conf\nginx.conf
goto end

:help
  echo #  %0 version 2024.05.30
  echo #    Run nginx.exe
  echo #
  echo #  command:
  echo #    nginx help
  echo #
  echo #  options:
  echo #    t task    // 列出当前存活的 Nginx 进程
  echo #    s start
  echo #    r reload
  echo #    q quit
  echo #    f fix     // 清除强退导致的 nginx.pid 文件残留
  echo #    o open    // 打开 http://localhost:8080
  echo #    i ip      // 打开 http://127.0.0.1:8080
  echo #    e edit    // 编辑 nginx.bat 脚本
  echo #    c config  // 编辑 nginx.conf 配置文件
goto end

:end